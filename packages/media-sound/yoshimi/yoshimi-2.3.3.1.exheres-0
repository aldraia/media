# Copyright 2010 Adriaan Leijnse <adriaan@leijnse.net>
# Based in part upon 'yoshimi-0.055.6.ebuild' which is:
#   Copyright 1999-2009 Gentoo Foundation
# Released under the terms of the GNU General Public License v2

require github \
    cmake \
    gtk-icon-cache

SUMMARY="Yoshimi is a software synthesizer based on ZynAddSubFX 2.4.0"
DESCRIPTION="
Yoshimi is a software synthesizer for Linux based on the 2.4.0 release of
ZynAddSubFX, written by Nasca Octavian Paul. Yoshimi delivers the same synth
capabilities, along with very good Jack and Alsa midi/audio functionality.
"
HOMEPAGE+=" https://${PN}.github.io"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    lv2 [[ description = [ Support for LV2 (simple but extensible successor of LADSPA) plugins ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/Mini-XML:=[>=2.5&<4]
        media-libs/fontconfig[>=0.22]
        media-libs/libsndfile
        media-sound/jack-audio-connection-kit[>=0.115.6]
        sci-libs/fftw[>=3.0.22]
        sys-libs/ncurses
        sys-libs/readline:=
        sys-libs/zlib
        sys-sound/alsa-lib[>=1.0.17]
        x11-libs/cairo[X]
        x11-libs/fltk
        lv2? ( media-libs/lv2[>=1.0.0] )
"

CMAKE_SOURCE=${WORKBASE}/${PNV}/src

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DJackSessionSupport:BOOL=TRUE
    -DBuildFor{0ld_X86,AMD_X86_64,Core2_X86_64,Debug,Diagnostic,RasPi4,ThisCPU}:BOOL=FALSE
    -DBuildOptions_Basic:STRING="${CFLAGS}"
    -DBuildWithFLTK:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    "lv2 LV2Plugin"
)

src_prepare() {
    cmake_src_prepare

    # fix docdir
    edo sed \
        -e "s:\${CMAKE_INSTALL_DATAROOTDIR}/doc/yoshimi:\${CMAKE_INSTALL_DATAROOTDIR}/doc/${PNVR}:g" \
        -i CMakeLists.txt
}

