# Copyright 2008 Thomas Anderson
# Copyright 2012 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A free ATSC A/52 stream decoder"
HOMEPAGE="https://git.adelielinux.org/community/a52dec/"
DOWNLOADS="https://distfiles.adelielinux.org/source/a52dec/a52dec-0.8.0.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    djbfft [[ description = [ Enable DJB FFT fallback for IMDCT ] ]]
    oss
"

DEPENDENCIES="
    build+run:
        djbfft? ( dev-libs/djbfft )
        oss? ( media-libs/libao )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-0.7.4-lto.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    "--enable-shared"

    # TODO(compnerd) determine what double precision floating point for audio playback via libaudio
    # means; stick with the default of disabled until then.
    "--disable-double"

    # platform specific audio backends
    "--disable-solaris-audio"
    "--disable-al-audio"
    "--disable-win"

    # disable profiling support by default
    "--disable-gprof"
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    # support OSS via libaudio
    "djbfft"
    "oss"
)

src_prepare() {
    default

    # Use correctly prefixed `nm`
    edo sed -e "/nm -g/s:nm:$(exhost --tool-prefix)nm:" \
            -i test/globals
}

