# Copyright 2014-2016 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=${PNV/-/_}

require cmake alternatives

export_exlib_phases src_prepare src_configure src_compile src_test src_install

SUMMARY="Command line application and library for encoding video streams into the H.265/High Efficiency Video Coding (HEVC) format"
HOMEPAGE="https://www.${PN}.org"
DOWNLOADS="https://bitbucket.org/multicoreware/${PN}_git/downloads/${MY_PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

# linking TestBench fails with: undefined reference to x265_{alloc,free}_analysis_data
# last checked: 3.0
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/nasm[>=2.13.0]
    run:
        !media-libs/x265:0[<3.1.2-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

CMAKE_SOURCE=${WORKBASE}/${MY_PNV}/source

# NOTE: Apparently 32-bit x86 together with HIGH_BIT_DEPTH are not supported by upstream.
# https://bitbucket.org/multicoreware/x265_git/issues/198/assembly-code-fails-on-x32
if [[ $(exhost --target) == i686-* ]]; then
    HIGH_BIT_DEPTH_ASM=0
else
    HIGH_BIT_DEPTH_ASM=1
fi

x265_src_prepare() {
    cmake_src_prepare

    edo mkdir "${WORKBASE}"/build-{10,12}
}

x265_src_configure() {
    local params=(
        -DENABLE_HDR10_PLUS:BOOL=FALSE
        -DENABLE_LIBNUMA:BOOL=FALSE
        -DENABLE_LIBVMAF:BOOL=FALSE
        -DENABLE_SVT_HEVC:BOOL=FALSE
        $(expecting_tests -DENABLE_TESTS:BOOL=TRUE)
    )

    # https://x265.readthedocs.io/en/master/api.html#multi-library-interface -DEXPORT_C_API is
    # turned off so the libraries can be loaded into the same process without symbol name
    # conflicts

    edo pushd "${WORKBASE}"/build-12
    ecmake \
        "${params[@]}" \
        -DEXPORT_C_API:BOOL=FALSE \
        -DENABLE_SHARED:BOOL=FALSE \
        -DENABLE_CLI:BOOL=FALSE \
        -DHIGH_BIT_DEPTH:BOOL=TRUE \
        -DMAIN12:BOOL=TRUE \
        $([[ ${HIGH_BIT_DEPTH_ASM} == 1 ]] || echo "-DENABLE_ASSEMBLY:BOOL=FALSE")
    edo popd

    edo pushd "${WORKBASE}"/build-10
    ecmake \
        "${params[@]}" \
        -DEXPORT_C_API:BOOL=FALSE \
        -DENABLE_SHARED:BOOL=FALSE \
        -DENABLE_CLI:BOOL=FALSE \
        -DHIGH_BIT_DEPTH:BOOL=TRUE \
        $([[ ${HIGH_BIT_DEPTH_ASM} == 1 ]] || echo "-DENABLE_ASSEMBLY:BOOL=FALSE")
    edo popd

    # Create empty 12bit and 10bit libraries so configure doesn't fail. We'll properly build them in
    # the next phase.
    edo ${AR} rc libx265_main10.a
    edo ${AR} rc libx265_main12.a

    # Now build the main library with 8-bit as default target
    params+=(
        -DLINKED_10BIT:BOOL=TRUE
        -DLINKED_12BIT:BOOL=TRUE
        -DEXTRA_LIB:STRING="x265_main10.a;x265_main12.a"
        -DEXTRA_LINK_FLAGS:STRING=-L.
    )

    ecmake "${params[@]}"
}

x265_src_compile() {
    edo pushd "${WORKBASE}"/build-12
    emake
    edo cp libx265.a "${WORKBASE}"/build/libx265_main12.a
    edo popd

    edo pushd "${WORKBASE}"/build-10
    emake
    edo cp libx265.a "${WORKBASE}"/build/libx265_main10.a
    edo popd

    default

    # Combine the libraries as in upstream's multilib.sh
    edo mv libx265{,_main}.a
    edo ${AR} -M <<EOF
CREATE libx265.a
ADDLIB libx265_main.a
ADDLIB libx265_main10.a
ADDLIB libx265_main12.a
SAVE
END
EOF
}

x265_src_test() {
    edo pushd "${WORKBASE}"/build-12
    edo ./test/TestBench
    edo popd

    edo pushd "${WORKBASE}"/build-10
    edo ./test/TestBench
    edo popd

    edo ./test/TestBench
}

x265_src_install() {
    local arch_dependent_alternatives=()
    local host=$(exhost --target)

    cmake_src_install

    arch_dependent_alternatives+=(
        /usr/${host}/bin/${PN}              ${PN}-${SLOT}
        /usr/${host}/include/${PN}.h        ${PN}-${SLOT}.h
        /usr/${host}/include/${PN}_config.h ${PN}_config-${SLOT}.h
        /usr/${host}/lib/lib${PN}.a         lib${PN}-${SLOT}.a
        /usr/${host}/lib/lib${PN}.so        lib${PN}-${SLOT}.so
        /usr/${host}/lib/pkgconfig/${PN}.pc ${PN}-${SLOT}.pc
    )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
}

