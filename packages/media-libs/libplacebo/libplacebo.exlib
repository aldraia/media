# Copyright 2019 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix='https://code.videolan.org' user='videolan' tag=v${PV} suffix=tar.bz2 new_download_scheme=true ]
require python [ blacklist=2 multibuild=false ]
require ffmpeg [ dep=false ]
require meson alternatives

export_exlib_phases src_install

SUMMARY="the core rendering algorithms and ideas of mpv turned into a library"

LICENCES="LGPL-2.1"
MYOPTIONS="
    lcms [[ description = [ Support for LittleCMS 2 ] ]]
    ( providers: shaderc glslang ) [[
        *description = [ Provide SPIR-V compiler ]
        number-selected = exactly-one
    ]]
"

DEPENDENCIES="
    build:
        dev-python/glad2[python_abis:*(-)?]
        dev-python/Jinja2[python_abis:*(-)?]
        dev-python/MarkupSafe[python_abis:*(-)?]
        virtual/pkg-config
    build+run:
        sys-libs/vulkan-headers[>=1.2]
        sys-libs/vulkan-loader[>=1.2]
        lcms? ( media-libs/lcms2[>=2.9] )
        providers:shaderc? (
            sys-libs/shaderc[>=2019.1]
        )
        providers:glslang? (
            dev-lang/glslang[>=7.8.2850] [[ note = [ PATCH_LEVEL 2763 ] ]]
        )
    test:
        $(print_ffmpeg_dependencies)
        media-libs/dav1d:=
    run:
        !media-libs/libplacebo:0[<3.104.0-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]

"

MESON_SOURCE="${WORKBASE}"/${PN}-v${PV}

MESON_SRC_CONFIGURE_PARAMS+=(
    -Dbench=false
    -Dd3d11=disabled
    -Ddebug-abort=false
    -Ddemos=false
    -Dfuzz=false
    -Dgl-proc-addr=enabled
    -Dopengl=enabled
    -Dunwind=disabled
    -Dvk-proc-addr=enabled
    -Dvulkan=enabled
    -Dvulkan-registry=/usr/share/vulkan/registry/vk.xml
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    lcms
    'providers:glslang glslang'
    'providers:shaderc shaderc'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

if ever at_least 6.338.1 ; then
    DEPENDENCIES+="
        build:
            dev-libs/libglvnd [[ note = [ KHR/khrplatform.h ] ]]
            dev-libs/xxHash
        build+run:
            sys-libs/vulkan-headers[>=1.3]
            sys-libs/vulkan-loader[>=1.3]
    "
    MESON_SRC_CONFIGURE_PARAMS+=(
        -Ddovi=disabled
        -Dlibdovi=disabled
        -Dxxhash=enabled
    )
fi

libplacebo_src_install() {
    meson_src_install

    local host=$(exhost --target) arch_dependent_alternatives=()

    arch_dependent_alternatives+=(
        /usr/${host}/include/${PN}          ${PN}-${SLOT}
        /usr/${host}/lib/${PN}.so           ${PN}-${SLOT}.so
        /usr/${host}/lib/pkgconfig/${PN}.pc ${PN}-${SLOT}.pc
    )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
}

